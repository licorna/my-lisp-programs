;;;; spam.asd

(asdf:defsystem #:spam
  :description "Describe spam here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:cl-ppcre #:pathnames)
  :components ((:file "package")
               (:file "spam")))
