;;;; pathnames.asd

(asdf:defsystem #:pathnames
  :description "Pathnames library from Practical Common Lisp"
  :author "Rodrigo Valin <licorna@gmail.com>"
  :license  "This code does not belong to me!"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "pathnames")))
